import './App.css';

import { LoginPage } from './Components/Login/LoginPage';
import { RegisterPage } from './Components/Register/RegisterPage';
import CyberAttacksOverview from './Components/CyberAttacks/CyberAttacksOverview';
import CyberAttacksArticle from './Components/CyberAttacks/CyberAttacksArticle';
import CyberAttacksEditArticle from './Components/CyberAttacks/CyberAttacksEditArticle'
import CyberProtectionOverview from './Components/CyberProtection/CyberProtectionOverview';
import CyberProtectionArticle from './Components/CyberProtection/CyberProtectionArticle';
import CyberProtectionEditArticle from './Components/CyberProtection/CyberProtectionEditArticle';
import CyberProtectionAddArticle from './Components/CyberProtection/CyberProtectionAddArticle';
import CyberAttacksAddArticle from './Components/CyberAttacks/CyberAttacksAddArticle';
import TrainingMaterial from './Components/TrainingMaterial/TrainingMaterial'
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LandingPage from './Components/LandingPage/LandingPage';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/Home" component={LandingPage} />
        <Route exact path="/CyberAttacks/:id/edit" component={CyberAttacksEditArticle} />
        <Route exact path="/CyberAttacks/add" component={CyberAttacksAddArticle} />
        <Route path="/CyberAttacks/:id" component={CyberAttacksArticle} />
        <Route exact path="/CyberAttacks" component={CyberAttacksOverview} />
        <Route exact path="/CyberProtections/:id/edit" component={CyberProtectionEditArticle} />
        <Route exact path="/CyberProtections/add" component={CyberProtectionAddArticle} />
        <Route path="/CyberProtections/:id" component={CyberProtectionArticle} />
        <Route path="/CyberProtections" component={CyberProtectionOverview} />
        <Route path="/TrainingMaterial" component={TrainingMaterial} />
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/register" component={RegisterPage} />
        <Route path="*" component={() => "404 NOT FOUND"} />
      </Switch>
    </div >
  );
}

export default App;
