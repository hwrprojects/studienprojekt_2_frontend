import React from 'react'
import reactDom from 'react-dom'

export default function Dialog({ open, children, onClose, confirmed }) {
    if (!open) return null

    return reactDom.createPortal(
        <div className='dialog'>
            <div className='dialog-content'>
                <div className='dialog-message'>
                    {children}
                </div>
                <div className='dialog-buttons-container'>
                    <button className='dialog-button' onClick={onClose}>No</button>
                    <button className='dialog-button yes' onClick={confirmed}>Yes</button>
                </div>
            </div>
        </div>,
        document.getElementById('portal')

    )
}
