import Sidebar from "../Sidebar/Sidebar"
import React from 'react';
import { Editor, EditorState, RichUtils, ContentState, convertToRaw, convertFromRaw } from 'draft-js';
import { useHistory } from 'react-router-dom'
import Dialog from './Dialog'
import 'draft-js/dist/Draft.css';
import './editor.css'

const EditArticle = (fetchCurrent, saveCurrentAndRedirect) => {
    let currentTextStyle = ""
    const history = useHistory();
    const [editorState, setEditorState] = React.useState(
        () => {
            var state = EditorState.createEmpty();
            return state;
        },
    );
    const [titleState, setTitleState] = React.useState([]);

    const [isOpen, setIsOpen] = React.useState(false)

    React.useEffect(() => {
        fetchCurrent().then(data => {
            console.log(data.description)
            if (data.description !== undefined) {
                setTitleState({ name: data.name })
                setEditorState(EditorState.createWithContent(convertFromRaw(JSON.parse(data.description))))
            } else {
                setTitleState('')
                setEditorState(EditorState.createEmpty())
            }
        })
    }, [])

    function MyEditor() {
        currentTextStyle = editorState.getCurrentInlineStyle()

        toggleButton(currentTextStyle, "BOLD")
        toggleButton(currentTextStyle, "ITALIC")
        toggleButton(currentTextStyle, "UNDERLINE")

        return (
            <Editor editorState={editorState} onChange={setEditorState} placeholder="Start writing your article here..." />
        );
    }

    function toggleButton(buttonStyle, styleType) {
        const button = document.getElementById(styleType)

        if (buttonStyle.has(styleType)) {
            if (button !== null && button !== 'undefined') {
                button.classList.remove("button_off")
                button.classList.add("button_on")
            }
        } else {
            if (button !== null && button !== 'undefined') {
                button.classList.remove("button_on")
                button.classList.add("button_off")
            }
        }
    }

    /* Toggle styles implementation taken from user Ashish Kamble 
    (https://stackoverflow.com/questions/49831728/clicking-on-button-is-not-adding-inline-style-using-draft-js)
    on 13.12.2021 */

    const _onMouseDown = (e, bstyle) => {
        e.preventDefault();
        setEditorState(RichUtils.toggleInlineStyle(editorState, bstyle))
    }

    return (
        <div className="Page">
            <Sidebar />
            <div className="Content_editor">
                <p className="back-button" onClick={() => { setIsOpen(true) }}>Go back to overview</p>
                <input type="text" className="heading" placeholder="Give your article a title here..." value={titleState.name} onChange={e => setTitleState({ name: e.target.value })}></input>
                <div className="buttons_container">
                    <button id="BOLD" className="button_off" onMouseDown={e => { _onMouseDown(e, "BOLD") }} >B</button>
                    <button id="ITALIC" className="button_off" onMouseDown={e => { _onMouseDown(e, "ITALIC") }}>I</button>
                    <button id="UNDERLINE" className="button_off" onMouseDown={e => { _onMouseDown(e, "UNDERLINE") }}>U</button>
                    <button className="button_save" onClick={() => saveCurrentAndRedirect(titleState.name, JSON.stringify(convertToRaw(editorState.getCurrentContent())))}>Speichern</button>
                </div>
                <MyEditor />
                <Dialog open={isOpen} onClose={() => setIsOpen(false)} confirmed={() => history.goBack()}>
                    Are you sure you want to go back?
                    Your changes won't be saved!
                </Dialog>
            </div>
        </div>
    )
}

export default EditArticle