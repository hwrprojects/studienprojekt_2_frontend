import EditButton from "../EditButton/EditButton"
import Sidebar from "../Sidebar/Sidebar"
import DeleteButton from "../DeleteButton/DeleteButton"
import { useState, useEffect } from 'react';
import { Editor, EditorState, RichUtils, ContentState, convertToRaw, convertFromRaw } from 'draft-js';
import { useHistory } from 'react-router-dom'

const Article = (title, content, type, objectId, deletefunction) => {
    const [editorState, setEditorState] = useState(
        () => {
            console.log(content)
            var state = EditorState.createEmpty()
            return state;
        },
    );

    const history = useHistory();

    useEffect(() => {
        if(content !== undefined){
            var state = EditorState.createWithContent(convertFromRaw(JSON.parse(content)))
        
            setEditorState(state);
        }
    }, [content])

    return (
        <div className="Page">
            <Sidebar />
            <div className="Content">
                <div className="Article">
                    <p className="back-button" onClick={() => {history.goBack()}}>Go back to overview</p>
                    <h1>
                        {title}
                        <EditButton />
                        {DeleteButton(() => deletefunction(objectId))}
                    </h1>
                    <h2>{type}</h2>
                    <Editor editorState={editorState} readOnly={true} />
                </div>
            </div>
        </div>
    )
}

export default Article