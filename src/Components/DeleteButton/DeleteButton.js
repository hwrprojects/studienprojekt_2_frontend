import auth from "../Login/auth"
import '../../App.css'
import { useHistory } from 'react-router-dom'
import { useState } from "react"
import Dialog from './../Article/Dialog'

const DeleteButton = (deletefunction) => {
    const history = useHistory();
    let visible = 'none'
    if (auth.isAuthenticated()) {
        visible = ''
    }

    const [isOpen, setIsOpen] = useState(false)

    function deleteAndRedirect() {
        deletefunction()
        history.goBack();
    }

    return (
        <>
        <button className="button_delete" style={{ display: visible }} onClick={() => setIsOpen(true)}>
            Löschen
        </button>
        <Dialog open={isOpen} onClose={() => setIsOpen(false)} confirmed={() => deleteAndRedirect()}>
            Are you sure you want to delete this article?
        </Dialog>
        </>
    )
}

export default DeleteButton
