import React from 'react'
import auth from '../Login/auth'
import { useHistory } from 'react-router-dom'
import '../../App.css'

const EditButton = () => {
    let visible = 'none'
    const history = useHistory();

    if (auth.isAuthenticated()) {
        visible = ''
    }

    return (
        <button className="button_edit" style={{display: visible}} onClick={() => onClick(history)}>
            Editieren
        </button>
    )
}

const onClick = (history) => {

    let path = window.location.pathname + '/edit';
    history.replace(path);
}

export default EditButton
