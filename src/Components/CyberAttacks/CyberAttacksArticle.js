import axios from 'axios';
import { useState, useEffect } from 'react';
import Article from "../Article/Article"

const CyberAttacksArticle = ({match}) => {
    const [CyberAttack, setCyberAttack] = useState([])
    const cyberAttackId = match.params.id
    useEffect(() => {
        fetchCyberAttack(setCyberAttack, cyberAttackId);
    }, [cyberAttackId])

    return (
        <div>
            {Article(CyberAttack.name, CyberAttack.description, CyberAttack.type, cyberAttackId, deleteCyberAttack)}
        </div>
    )
}


const fetchCyberAttack = (setCyberAttack, cyberAttackId) =>  {
    console.log({cyberAttackId})
    console.log(`http://localhost:3000/cyberAttacks/${cyberAttackId}`)
    axios.get(`http://localhost:3000/cyberAttacks/${cyberAttackId}`)
        .then(response => {
            console.log(response)
            setCyberAttack(response.data);
        })
        .catch(err => {console.log(err)})
}
const deleteCyberAttack = (cyberAttackId) =>  {
    console.log(`http://localhost:3000/cyberAttacks/${cyberAttackId}`)
    axios.delete(`http://localhost:3000/cyberAttacks/${cyberAttackId}`)
        .then(response => {
            console.log(response)
        })
        .catch(err => {console.log(err)})
}

export default CyberAttacksArticle
