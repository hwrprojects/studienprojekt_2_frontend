import axios from 'axios';
import EditArticle from "../Article/EditArticle"
import { useHistory } from 'react-router-dom'

const CyberAttackEditArticle = ({ match }) => {
    const cyberAttackId = match.params.id
    const history = useHistory();

    return (
        <div>
            {EditArticle(() => fetchCyberAttack(cyberAttackId), (title, description) => updateCyberAttackAndRedirect(cyberAttackId, title, description, history))}
        </div>
    )
}

const fetchCyberAttack = async (cyberAttackId) => {
    console.log(`http://localhost:3000/cyberAttacks/${cyberAttackId}`)
    try {
        const response = await axios.get(`http://localhost:3000/cyberAttacks/${cyberAttackId}`);
        return response.data;
    } catch (err) {
        console.log(err);
    }
}

const updateCyberAttackAndRedirect = async (cyberAttackId, name, description, history) => {
    console.log(cyberAttackId)
    console.log(name)
    console.log(description)
    try {
        axios.patch(process.env.REACT_APP_BACKEND_BASE_URL + `/cyberAttacks/${cyberAttackId}`, {
            "name": name,
            "description": description
        }
        ).then(() => {
            let path = window.location.pathname;
            path = path.substring(0, path.indexOf('/edit'));
            history.replace(path);
            return true;
        })
    } catch (err) {
        console.log(err);
        return false;
    }
}
export default CyberAttackEditArticle
