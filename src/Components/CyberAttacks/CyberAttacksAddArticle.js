import axios from 'axios';
import EditArticle from "../Article/EditArticle"
import { useHistory } from 'react-router-dom'

const CyberAttacksAddArticle = ({match}) => {
    const history = useHistory();

    return (
        <div>
            {EditArticle(async() => {return ""}, (name, description) => addCyberAttackAndRedirect(name, description, history))}
        </div>
    )
}

const addCyberAttackAndRedirect = async(name, description, history) => {
    console.log(description)
    try{
        axios.post(process.env.REACT_APP_BACKEND_BASE_URL + `/cyberAttacks`, {
        "name": name,
        "description": description
    }
    ).then((res) => {
        let path = window.location.pathname;
        path = path.substring( 0, path.indexOf( '/add' ) );
        path = path+'/'+res.data._id;
        history.replace(path);
        return true;
    })
    } catch (err) {
        console.log(err);
        return false;
    }
}
export default CyberAttacksAddArticle
