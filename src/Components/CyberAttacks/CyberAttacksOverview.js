import '../../App.css';
import '../LandingPage/LandingPage.css';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom'
import Sidebar from '../Sidebar/Sidebar';
import auth from '../Login/auth';
import AddButton from '../AddButton/AddButton';

const CyberAttacksOverview = () => {
    const [CyberAttacks, setCyberAttacks] = useState([])

    const history = useHistory();

    useEffect(() => {
        fetchCyberAttacks(setCyberAttacks);
    }, [])

    function handleClickCyberAttacks(props) {
        let path = `/CyberAttacks/${props}`;
        history.push(path);
    }

    return (
        <div className="Page">
            {console.log(auth.isAuthenticated())}
            <Sidebar />
            <div className="Content" id="cards">
                <h1> List of CyberAttacks <AddButton /></h1>
                {CyberAttacks.map((attack) => {
                    return (
                            <div className="card" key={attack._id} onClick={() => handleClickCyberAttacks(attack._id)}>
                                <div className="card-content">
                                    <h2 className="card-title">{attack.name}</h2>
                                    <p className="card-body">Press to find more information.</p>
                                </div>
                            </div>)
                }
                )}
            </div>
        </div>
    )
}

const fetchCyberAttacks = (setCyberAttacks) => {
    axios.get("http://localhost:3000/cyberAttacks")
        .then(response => {
            console.log(response)
            setCyberAttacks(response.data);
        })
        .catch(err => { console.log(err) })
}



export default CyberAttacksOverview
