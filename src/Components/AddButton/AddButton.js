import React from 'react'
import auth from '../Login/auth'
import { useHistory } from 'react-router-dom'
import '../../App.css'

const AddButton = () => {
    let visible = 'none'
    const history = useHistory();

    if (auth.isAuthenticated()) {
        visible = ''
    }

    return (
        <button className="button_add" style={{display: visible}} onClick={() => onClick(history)}>
            Hinzufügen
        </button>
    )
}

const onClick = (history) => {

    let path = window.location.pathname + '/add';
    history.push(path);
}

export default AddButton