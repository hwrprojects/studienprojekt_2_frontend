const robotAttackEmoji = '🤖'
const robotDefendEmoji = '🛡️'
const trainingMaterialEmoji = '🥋'
const homeEmoji = '🏠'

export const SidebarOptions = [
    {
        title: 'Home',
        emoji: homeEmoji,
        link: '/Home'
    },
    {
        title: 'Cyber Attacks',
        emoji: robotAttackEmoji,
        link: '/CyberAttacks'
    },
    {
        title: 'Cyber Protections',
        emoji: robotDefendEmoji,
        link: '/CyberProtections'
    },
    {
        title: 'Training Material',
        emoji: trainingMaterialEmoji,
        link: '/TrainingMaterial'
    }
];
