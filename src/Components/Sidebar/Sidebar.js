import { SidebarOptions } from './SidebarOptions';
import auth from '../Login/auth';
import { Link } from 'react-router-dom';
import '../../App.css';

const Sidebar = () => {
    return (
        <div className="Sidebar">
            {checkAuthentication()}
            {getSidebarTitle()}
            {mapSidebarOptionsToSidebarList()}
        </div>
    )
}

let logout = 'none';
let login = '';

function checkAuthentication() {
    if (auth.isAuthenticated()) {
        logout = ''
        login = 'none'
        console.log("page reloaded")
    }
}

function refreshPage() {
    window.location.reload(false);
}

function checkSelected(url, value) {
    switch (value) {
        case "/Home":
            if (url.includes(value) || url === '/') {
                return 'selected'
            } return ''
        case "/CyberAttacks":
            if (url.includes(value)) {
                return 'selected'
            } return ''
        case "/CyberProtections":
            if (url.includes(value)) {
                return 'selected'
            } return ''
        case "/TrainingMaterial":
            if (url.includes(value)) {
                return 'selected'
            } return ''

        default:
            return ''
    }
}

const mapSidebarOptionsToSidebarList = () => {
    const currentUrl = window.location.href.substring(window.location.href.indexOf('0') + 3)
    
    return (
        <ol>
            {SidebarOptions.map((value, key) => {

                return (
                    <li key={value.title}>
                        <Link to={value.link} className={checkSelected(currentUrl, value.link)}>
                            <div>
                                {value.emoji}
                            </div>
                            <div>
                                {value.title}
                            </div>
                        </Link>
                    </li>
                )
            })}
            <li key={'logout'} style={{ display: logout }}>
                <Link
                    to='/'
                    onClick={() => {
                        auth.logout(() => {
                            refreshPage();
                        });
                    }}
                >
                    <div>🚪</div>
                    <div>Logout</div>
                </Link>
            </li>
            <li key={'login'} style={{ display: login }}>
                <Link
                    to='/login'
                >
                    <div>🚪</div>
                    <div>Login</div>
                </Link>
            </li>
        </ol>
    )
}

const getSidebarTitle = () => {
    return (
        <h3>Cybersecurity</h3>
    )
}

export default Sidebar
