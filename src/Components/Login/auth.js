import axios from 'axios';
class Auth {
    constructor() {
      this.authenticated = false;
    }
  
    login(cb) {
      this.authenticated = true;
      cb();
    }
  
    logout(cb) {
      axios.delete((process.env.REACT_APP_BACKEND_BASE_URL + "/Users/logout"))
        .then(response => {
        console.log(response)
        if(response.status === 205) {
          console.log("Erfolgreich ausgeloggt")
        }else {
          console.log("Fehler beim ausloggen")
        }
        })
        .catch(err => { console.log(err) })
          this.authenticated = false;
          cb();
        }
  
    isAuthenticated() {
      return this.authenticated;
    }
  }
  
  export default new Auth();
  