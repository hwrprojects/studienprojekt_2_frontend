import { useState } from 'react'
import auth from './auth';
import '../../Login.css';
import axios from 'axios';
import Snackbar from '../Notification/Snackbar';
import { useRef } from 'react';


export const LoginPage = props => {
    const [details, setDetails] = useState({ name: "", email: "", password: "" });

    const [user, setuser] = useState({ name: "", email: "" });
    const [error, seterror] = useState("");

    const submitHandler = e => {
        e.preventDefault();
        Login(details);
    }

    //Notification
    const snackbarRef = useRef(null);

    const SnackbarType = {
        success: "success",
        fail: "fail"
    }

    const Login = details => {
        axios.post((process.env.REACT_APP_BACKEND_BASE_URL + "/Users/login"), {
            "mail": details.email,
            "password": details.password,
        }
        )
            .then(response => {
                if (response.status === 200) {
                    setuser({
                        name: details.name,
                        email: details.email
                    });
                    auth.login(() => {
                        props.history.push("/home")
                    })
                } else {
                    console.log("Details do not match!");
                    snackbarRef.current.show();
                }
            })
            .catch(err => { console.log(err) })
    }

    return (
        <form onSubmit={submitHandler}>
            <div className="form-inner">
                <h2>Login</h2>
                {(error !== "") ? (<div className="error">{error}</div>) : ""}
                <div className="form-group">
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" onChange={e => setDetails({ ...details, email: e.target.value })} value={details.email} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" id="password" onChange={e => setDetails({ ...details, password: e.target.value })} value={details.password} />
                </div>
                <div className="register" onClick={() => props.history.push("/register")}>
                    <p>No account? Press here to register.</p>
                </div>
                <input type="submit" value="Login" />
            </div>
            <Snackbar ref={snackbarRef} message="Details do not match!" type={SnackbarType.fail} />
        </form>

    )
};
