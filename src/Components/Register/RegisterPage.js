import React, { useState } from 'react'
import auth from '../Login/auth';
import '../../Login.css';
import Snackbar from '../Notification/Snackbar';
import { useRef } from 'react';
import axios from 'axios';

export const RegisterPage = props => {
    const [details, setDetails] = useState({ name: "", email: "", password: "" });


    const [user, setuser] = useState({ name: "", email: "" });
    const [error, seterror] = useState("");

    const submitHandler = e => {
        e.preventDefault();
        Login(details);
    }

    const Login = details => {
        console.log(details);
        axios.post((process.env.REACT_APP_BACKEND_BASE_URL + "/Users"), {
            "username": details.name,
            "mail": details.email,
            "password": details.password
        }
    )
    .then(response => {
        console.log(response)
        if(response.status === 200) {
            setuser({
                name: details.name,
                email: details.email
            });          
            auth.login(() => {
                props.history.push("/home")
            })
        }else {
            console.log("Details do not match!");
            snackbarRef.current.show();
        }
    })
    .catch(err => { console.log(err) })
    }

    //Notification
    const snackbarRef = useRef(null);

    const SnackbarType = {
        success: "success",
        fail: "fail"
    }

    return (
        <form onSubmit={submitHandler}>
            <div className="form-inner">
                <h2>Register</h2>
                {(error !== "") ? ( <div className="error">{error}</div> ) : ""}
                <div className="form-group">
                    <label htmlFor="name">Name:</label>
                    <input type="text" name="name" id="name" onChange={e => setDetails({ ...details, name: e.target.value })} value={details.name} />
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email:</label>
                    <input type="email" name="email" id="email" onChange={e => setDetails({ ...details, email: e.target.value })} value={details.email} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" id="password" onChange={e => setDetails({ ...details, password: e.target.value })} value={details.password} />
                </div>
                <div className="register" onClick={() => props.history.push("/login")}>
                    <p>Return to login.</p>
                </div>
                <input type="submit" value="Register" />
            </div>
            <Snackbar ref={snackbarRef} message="Details do not match!" type={SnackbarType.fail}/>
        </form>
    )
};
