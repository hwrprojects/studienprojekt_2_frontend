import axios from 'axios';
import { useState, useEffect } from 'react';
import Article from "../Article/Article"

const CyberProtectionArticle = ({match}) => {
    const [CyberProtection, setCyberProtection] = useState([])
    const cyberProtectionId = match.params.id
    useEffect(() => {
        fetchCyberProtection(setCyberProtection, cyberProtectionId);
    }, [])

    return (
        <div>
            {Article(CyberProtection.name, CyberProtection.description, CyberProtection.type, cyberProtectionId, deleteCyberProtection)}
        </div>
    )
}

const fetchCyberProtection = (setCyberProtection, cyberProtectionId) =>  {
    console.log({cyberProtectionId})
    console.log(`http://localhost:3000/cyberProtections/${cyberProtectionId}`)
    axios.get(`http://localhost:3000/cyberProtections/${cyberProtectionId}`)
        .then(response => {
            console.log(response)
            setCyberProtection(response.data);
        })
        .catch(err => {console.log(err)})
}

const deleteCyberProtection = (cyberProtectionId) =>  {
    axios.delete(`http://localhost:3000/cyberProtections/${cyberProtectionId}`)
        .then(response => {
            console.log(response)
        })
        .catch(err => {console.log(err)})
}

export default CyberProtectionArticle
