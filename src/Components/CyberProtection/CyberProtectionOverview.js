import '../../App.css';
import '../LandingPage/LandingPage.css';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom'
import Sidebar from '../Sidebar/Sidebar';
import auth from '../Login/auth';
import AddButton from '../AddButton/AddButton';

const CyberProtectionsOverview = () => {
    const [CyberProtections, setCyberProtections] = useState([])

    const history = useHistory();

    useEffect(() => {
        fetchCyberProtections(setCyberProtections);
    }, [])

    function handleClickCyberProtections(props) {
        let path = `/CyberProtections/${props}`;
        history.push(path);
    }

    return (
        <div className="Page">
            {console.log(auth.isAuthenticated())}
            <Sidebar />
            <div className="Content" id="cards">
                <h1>Protections against Cyber Attacks <AddButton /></h1>
                {CyberProtections.map((protection) => {
                    return (
                            <div className="card" key={protection._id} onClick={() => handleClickCyberProtections(protection._id)}>
                                <div className="card-content">
                                    <h2 className="card-title">{protection.name}</h2>
                                    <p className="card-body">Press to find more information.</p>
                                </div>
                            </div>)
                }
                )}
            </div>
        </div>
    )
}

const fetchCyberProtections = (setCyberProtections) => {
    axios.get(process.env.REACT_APP_BACKEND_BASE_URL +  "/cyberProtections")
        .then(response => {
            console.log(response)
            setCyberProtections(response.data);
        })
        .catch(err => { console.log(err) })
}



export default CyberProtectionsOverview
