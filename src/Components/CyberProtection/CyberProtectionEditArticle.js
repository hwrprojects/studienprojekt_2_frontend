import axios from 'axios';
import EditArticle from "../Article/EditArticle"
import { useHistory } from 'react-router-dom'

const CyberProtectionEditArticle = ({ match }) => {
    const cyberProtectionId = match.params.id
    const history = useHistory();
    return (
        <div>
            {EditArticle(() => fetchCyberProtection(cyberProtectionId), (name, description) => { updateCyberProtection(cyberProtectionId, name, description, history) })}
        </div>
    )
}

const fetchCyberProtection = async (cyberProtectionId) => {
    console.log({ cyberProtectionId })
    console.log(`http://localhost:3000/cyberProtections/${cyberProtectionId}`)
    try {
        const response = await axios.get(`http://localhost:3000/cyberProtections/${cyberProtectionId}`);
        return response.data;
    } catch (err) {
        console.log(err);
    }
}


const updateCyberProtection = async (cyberProtectionId, name, description, history) => {
    console.log(cyberProtectionId)
    console.log(description)
    try {
        axios.patch(process.env.REACT_APP_BACKEND_BASE_URL + `/cyberProtections/${cyberProtectionId}`, {
            "name": name,
            "description": description
        }
        ).then(() => {
            let path = window.location.pathname;
            path = path.substring(0, path.indexOf('/edit'));
            history.replace(path);
            return true;
        }
        )
    } catch (err) {
        console.log(err);
        return false;
    }
}


export default CyberProtectionEditArticle
