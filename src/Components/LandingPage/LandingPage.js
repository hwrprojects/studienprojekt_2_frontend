import '../../App.css';
import './LandingPage.css';
import { useHistory } from 'react-router-dom'
import Sidebar from '../Sidebar/Sidebar';
import auth from '../Login/auth';
import Snackbar from '../Notification/Snackbar';
import { useRef } from 'react';

const LandingPage = () => {
    const history = useHistory();

    function handleClickCyberAttacks() {
        let path = '/CyberAttacks';
        history.push(path);
    }

    function handleClickCyberSecurity() {
        let path = '/CyberProtections';
        history.push(path);
    }

    function handleClickTrainingMaterial() {
        let path = '/TrainingMaterial';
        history.push(path);
    }

    //Notification
    const snackbarRef = useRef(null);

    const SnackbarType = {
        success: "success",
        fail: "fail"
    }

    function handleClickLogin() {
        if (auth.isAuthenticated()) {
            console.log("log");
            snackbarRef.current.show();
        } else {
            let path = '/Login';
            history.push(path);
        }
    }

    return (
        <div className="Page">
            <Sidebar />
            <div className="Content" id="cards">
                <h1 className="title">Welcome to the Cyber Security Wiki</h1>
                <div className="card" onClick={() => handleClickCyberAttacks()}>
                    <div className="card-content">
                        <h2 className="card-title">Cyber Attacks</h2>
                        <p className="card-body">You can view a list of documented cyber attacks here.</p>
                    </div>
                </div>
                <div className="card" onClick={() => handleClickCyberSecurity()}>
                    <div className="card-content">
                        <h2 className="card-title">Cyber Protections</h2>
                        <p className="card-body">You can view a list of cyber security measures here.</p>
                    </div>
                </div>
                <div className="card" onClick={() => handleClickTrainingMaterial()}>
                    <div className="card-content">
                        <h2 className="card-title">Training Material</h2>
                        <p className="card-body">Here you can find training materials used for education purposes.</p>
                    </div>
                </div>
                <div className="card" onClick={() => handleClickLogin()}>
                    <div className="card-content">
                        <h2 className="card-title">Login</h2>
                        <p className="card-body">You can login here in order to add, remove or change information on this wiki.</p>
                    </div>
                </div>
                <Snackbar ref={snackbarRef} message="You are already logged in" type={SnackbarType.success}/>
            </div>
        </div>
    )
}



export default LandingPage
